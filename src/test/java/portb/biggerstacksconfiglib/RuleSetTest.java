/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.biggerstacksconfiglib;

import com.thoughtworks.xstream.converters.ConversionException;
import org.junit.jupiter.api.Test;
import portb.configlib.ConfigLib;
import portb.configlib.ItemProperties;
import portb.configlib.xml.Operator;
import portb.configlib.xml.RuleSet;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static portb.biggerstacksconfiglib.Util.*;

public class RuleSetTest
{
    //todo test item categories
    
    static final ItemProperties itemEmptyBucket = new ItemProperties(
            "minecraft",
            "minecraft:bucket",
            "misc",
            1,
            false,
            false,
            false,
            true,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    static final ItemProperties itemWaterBucket = new ItemProperties(
            "minecraft",
            "minecraft:water_bucket",
            "misc",
            1,
            false,
            false,
            false,
            true,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    static final ItemProperties itemSnowball = new ItemProperties(
            "minecraft",
            "minecraft:snowball",
            "misc", //don't know what it actually is
            16,
            false,
            false,
            false,
            false,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    static final ItemProperties itemCobblestone = new ItemProperties(
            "minecraft",
            "minecraft:cobblestone",
            "blocks",
            //i don't know what the actual value is in game
            64,
            false,
            true,
            false,
            false,
            Arrays.asList("forge:cobblestone", "forge:cobblestone/normal"),
            //https://forge.gemwire.uk/wiki/Tags#Forge_Tags
            RuleSetTest.class
    );
    
    static final ItemProperties itemBread = new ItemProperties(
            "minecraft",
            "minecraft:bread",
            "food", //i don't know what the actual value is in game
            64,
            true,
            false,
            false,
            false,
            Collections.emptyList(), //i don't think bread has any tags
            RuleSetTest.class
    );
    
    static final ItemProperties itemPickaxe = new ItemProperties(
            "minecraft",
            "minecraft:diamond_pickaxe",
            "tools", //i don't know what the actual value is in game
            1,
            false,
            false,
            true,
            false,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    static final ItemProperties itemDiamondShovel = new ItemProperties(
            "minecraft",
            "minecraft:diamond_shovel",
            "tools", //i don't know what the actual value is in game
            1,
            false,
            false,
            true,
            false,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    
    static final ItemProperties itemTwoStackSize = new ItemProperties(
            "supercraft",
            "supercraft:thing_that_can_only_have_two_items_in_a_stack_and_is_food",
            "???",
            2,
            true,
            false,
            false,
            false,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    static final ItemProperties itemImaginaryModdedItem = new ItemProperties(
            "supercraft",
            "supercraft:thing",
            "???",
            35,
            false,
            false,
            false,
            false,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    @Test
    void testTooManyEntities()
    {
        assertExceptionMessageStartsWith("Too many entities in \"somebody once told me\"",
                                         assertThrows(ConversionException.class,
                                                      () -> simpleRuleset("somebody once told me")
                                         )
        );
    }
    
    @Test
    void testPreventStackingDamageableItems()
    {
        RuleSet rule = ConfigLib.parseRuleset("<ruleset>" +
                                                      "<rule stacksize=\"1\"> <condition>is_tool</condition> </rule> " +
                                                      "<rule stacksize=\"100\"> <condition>id minecraft:diamond_pickaxe</condition> </rule>" +
                                                      "</ruleset>");
        
        Optional<Integer> stackSize = rule.determineStackSizeForItem(itemPickaxe);
        
        assertTrue(stackSize.isPresent());
        assertEquals(stackSize.get(), 1);
    }
    
    //mod does not do this any more
//    @Test
//    void testPreventStackingBuckets()
//    {
//        RuleSet rule = simpleRuleset("id != minecraft:sand");
//
//        assertItemMatch(itemEmptyBucket, rule);
//        assertItemFail(itemWaterBucket, rule);
//    }
    
    @Test
    void testOrBlock()
    {
        RuleSet rule = ConfigLib.parseRuleset(
                "<ruleset>" +
                        "<rule stacksize=\"100\">" +
                        "<or>" +
                        "<condition>id minecraft:cobblestone</condition>" +
                        "<condition>id minecraft:snowball</condition>" +
                        "</or>" +
                        "</rule>" +
                        "</ruleset>");
        
        assertItemMatch(itemCobblestone, rule);
        assertItemMatch(itemSnowball, rule);
        assertItemFail(itemBread, rule);
        assertItemFail(itemImaginaryModdedItem, rule);
    }
    
    @Test
    void testTagIncludes()
    {
        RuleSet rule = simpleRuleset("tag includes forge:cobblestone/normal");
        
        assertItemMatch(itemCobblestone, rule);
        assertItemFail(itemSnowball, rule);
        assertItemFail(itemTwoStackSize, rule);
        assertItemFail(itemImaginaryModdedItem, rule);
    }
    
    @Test
    void testTagExcludes()
    {
        RuleSet rule = simpleRuleset("tag excludes forge:cobblestone/normal");
        
        assertItemFail(itemCobblestone, rule);
        assertItemMatch(itemSnowball, rule);
        assertItemMatch(itemTwoStackSize, rule);
        assertItemMatch(itemImaginaryModdedItem, rule);
    }
    
    @Test
    void testGreaterThan()
    {
        RuleSet rule = simpleRuleset("stacksize gt 16");
        
        assertItemMatch(itemBread, rule);
        assertItemMatch(itemImaginaryModdedItem, rule);
        assertItemFail(itemTwoStackSize, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testGreaterThanOrEqual()
    {
        RuleSet rule = simpleRuleset("stacksize gte 35");
        
        assertItemMatch(itemBread, rule);
        assertItemMatch(itemImaginaryModdedItem, rule);
        assertItemFail(itemTwoStackSize, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testLessThan()
    {
        RuleSet rule = simpleRuleset("stacksize lt 17");
        
        assertItemFail(itemBread, rule);
        assertItemFail(itemImaginaryModdedItem, rule);
        assertItemMatch(itemTwoStackSize, rule);
        assertItemMatch(itemSnowball, rule);
    }
    
    @Test
    void testLessThanOrEqual()
    {
        RuleSet rule = simpleRuleset("stacksize lte 16");
        
        assertItemFail(itemBread, rule);
        assertItemFail(itemImaginaryModdedItem, rule);
        assertItemMatch(itemTwoStackSize, rule);
        assertItemMatch(itemSnowball, rule);
    }
    
    @Test
    void testBoolean()
    {
        RuleSet rule = simpleRuleset("is_food = true");
        
        assertItemMatch(itemBread, rule);
        assertItemFail(itemCobblestone, rule);
    }
    
    @Test
    void testMarshalling()
    {
        //the xml header is added by xstream
        //the custom converter also writes conditions in formal syntax
        String xml = "<?xml version=\"1.0\" ?><ruleset><rule stacksize=\"100\"><or><condition>id = minecraft:cobblestone</condition><condition>id = minecraft:snowball</condition></or></rule></ruleset>";
        
        assertEquals(xml, ConfigLib.xStream().toXML(ConfigLib.parseRuleset(xml)));
    }
    
    @Test
    void testInvalidOperator()
    {
        assertExceptionMessageStartsWith("Operator \"when\" is invalid",
                                         assertThrows(ConversionException.class,
                                                      () -> simpleRuleset("tag when forge:glass")
                                         )
        );
    }
    
    /**
     * Test single form for property that doesn't exist
     */
    @Test
    void testSingleEntityFormInvalidProperty()
    {
        assertExceptionMessageStartsWith("Property \"kojima\" is invalid",
                                         assertThrows(ConversionException.class, () -> simpleRuleset("kojima"))
        );
    }
    
    /**
     * Test single form for a property that doesn't support single form
     */
    @Test
    void testSingleEntityFormUnsupportedProperty()
    {
        assertExceptionMessageStartsWith("Property \"stacksize\" requires a value to compare to",
                                         assertThrows(ConversionException.class, () -> simpleRuleset("stacksize"))
        );
    }
    
    @Test
    void testSingleEntityFormValid()
    {
        RuleSet rule = simpleRuleset("is_food");
        
        assertItemMatch(itemBread, rule);
        assertItemMatch(itemTwoStackSize, rule);
        assertItemFail(itemCobblestone, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testDoubleEntityFormUsingPropertyThatDoesNotExist()
    {
        assertExceptionMessageStartsWith("Property \"kojima\" is invalid",
                                         assertThrows(ConversionException.class, () -> simpleRuleset("kojima 10"))
        );
    }
    
    @Test
    void testDoubleEntityFormValid1()
    {
        RuleSet rule = assertDoesNotThrow(() -> simpleRuleset("stacksize 64"));
        
        assertItemMatch(itemBread, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testDoubleEntityFormValid2()
    {
        RuleSet rule = assertDoesNotThrow(() -> simpleRuleset("tag forge:cobblestone"));
        
        assertItemMatch(itemCobblestone, rule);
        assertItemFail(itemSnowball, rule);
        assertItemFail(itemTwoStackSize, rule);
        assertItemFail(itemImaginaryModdedItem, rule);
    }
    
    @Test
    void testStartsWithOperator()
    {
        RuleSet rule = assertDoesNotThrow(() -> simpleRuleset("id starts_with minecraft:diamond"));
        
        assertItemMatch(itemDiamondShovel, rule);
        assertItemMatch(itemPickaxe, rule);
        assertItemFail(itemCobblestone, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testEndsWithOperator()
    {
        RuleSet rule = assertDoesNotThrow(() -> simpleRuleset("id ends_with pickaxe"));
        
        assertItemFail(itemDiamondShovel, rule);
        assertItemMatch(itemPickaxe, rule);
    }
    
    @Test
    void testFullFormUsingPropertyThatDoesNotExist()
    {
        assertExceptionMessageStartsWith("Property \"kojima\" is invalid",
                                         assertThrows(ConversionException.class, () -> simpleRuleset("kojima = 10"))
        );
    }
    
    @Test
    void testInvalidOperatorNumeric()
    {
        testInvalidOperators(Arrays.asList(Operator.INCLUDES, Operator.EXCLUDES), "stacksize");
    }
    
    @Test
    void testInvalidOperatorTag()
    {
        testInvalidOperators(Arrays.asList(Operator.EQUALS,
                                           Operator.NOT_EQUALS,
                                           Operator.GREATER_THAN,
                                           Operator.GREATER_THAN_EQUAL,
                                           Operator.LESS_THAN,
                                           Operator.LESS_THAN_EQUAL
        ), "tag");
    }
    
    @Test
    void testInvalidOperatorBool()
    {
        List<Operator> invalidOperators = Arrays.asList(Operator.INCLUDES,
                                                        Operator.EXCLUDES,
                                                        Operator.GREATER_THAN,
                                                        Operator.GREATER_THAN_EQUAL,
                                                        Operator.LESS_THAN,
                                                        Operator.LESS_THAN_EQUAL
        );
        
        testInvalidOperators(invalidOperators, "is_food");
        testInvalidOperators(invalidOperators, "is_block");
    }
    
    @Test
    void testInvalidOperatorString()
    {
        List<Operator> invalidOperators = Arrays.asList(Operator.INCLUDES,
                                                        Operator.EXCLUDES,
                                                        Operator.GREATER_THAN,
                                                        Operator.GREATER_THAN_EQUAL,
                                                        Operator.LESS_THAN,
                                                        Operator.LESS_THAN_EQUAL
        );
        
        testInvalidOperators(invalidOperators, "mod");
        testInvalidOperators(invalidOperators, "id");
        testInvalidOperators(invalidOperators, "category");
    }
    
    void testInvalidOperators(List<Operator> operators, String propertyName)
    {
        for (Operator operator : operators)
        {
            assertExceptionMessageStartsWith(
                    "Operator \"" + operator.symbol() + "\" is invalid or can't be used with property \"" +
                            propertyName + "\"",
                    assertThrows(ConversionException.class,
                                 () -> simpleRuleset(propertyName + " " + operator.symbol() + " doesnt_matter")
                    )
            );
        }
    }
    
    
}
