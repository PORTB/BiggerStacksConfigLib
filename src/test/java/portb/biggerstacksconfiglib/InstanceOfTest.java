/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.biggerstacksconfiglib;

import org.junit.jupiter.api.Test;
import portb.configlib.ItemProperties;
import portb.configlib.xml.RuleSet;

import java.util.Collections;

import static portb.biggerstacksconfiglib.Util.*;

public class InstanceOfTest
{
    private final static String PACKAGE_NAME = "portb.biggerstacksconfiglib.InstanceOfTest";
    
    interface ThirdInterface {}
    
    interface SecondInterface {}
    
    static class SecondClass implements ThirdInterface {}
    
    //because java encourages you to wrap things in 100000000 layers of wrappers
    static class ItemWrapper extends SecondClass implements SecondInterface {}
    
    static class AbstractedItem extends ItemWrapper {}
    
    static final ItemProperties itemSnowball = new ItemProperties(
            "minecraft",
            "minecraft:snowball",
            "misc", //don't know what it actually is
            16,
            false,
            false,
            false,
            false,
            Collections.emptyList(),
            RuleSetTest.class
    );
    
    static final ItemProperties itemWithAbstractType = new ItemProperties(
            "java",
            "potion_of_death_by_abstraction",
            "thing",
            64,
            false,
            false,
            false,
            false,
            Collections.emptyList(),
            AbstractedItem.class
    );
    
    @Test
    void testInstanceClassNameEquals()
    {
        RuleSet rule = simpleRuleset("class = " + PACKAGE_NAME + "$AbstractedItem");
        
        assertItemMatch(itemWithAbstractType, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testInstanceClassNameNotEquals()
    {
        //equals operator should only match the actual class name, not the name of any interfaces or superclasses
        RuleSet rule = simpleRuleset("class = " + PACKAGE_NAME + "$ItemWrapper");
        
        assertItemFail(itemWithAbstractType, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testInstanceOf1stOrder()
    {
        RuleSet rule = simpleRuleset("class instanceof " + PACKAGE_NAME + "$AbstractedItem");
        
        assertItemMatch(itemWithAbstractType, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testInstanceOf2ndOrder()
    {
        RuleSet rule = simpleRuleset("class instanceof " + PACKAGE_NAME + "$SecondClass");
        
        assertItemMatch(itemWithAbstractType, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    @Test
    void testInstanceOf2ndOrderInterface()
    {
        RuleSet rule = simpleRuleset("class instanceof " + PACKAGE_NAME + "$SecondInterface");
        
        assertItemMatch(itemWithAbstractType, rule);
        assertItemFail(itemSnowball, rule);
    }
    
    
    @Test
    void testInstanceOf3rdOrder()
    {
        RuleSet rule = simpleRuleset("class instanceof " + PACKAGE_NAME + "$ThirdInterface");
        
        assertItemMatch(itemWithAbstractType, rule);
        assertItemFail(itemSnowball, rule);
    }
}
