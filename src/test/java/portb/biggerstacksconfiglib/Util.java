/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.biggerstacksconfiglib;

import portb.configlib.ConfigLib;
import portb.configlib.ItemProperties;
import portb.configlib.xml.RuleSet;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class Util
{
    static void assertItemMatch(ItemProperties item, RuleSet rule)
    {
        Optional<Integer> result = rule.determineStackSizeForItem(item);
        assertTrue(result.isPresent(), "Item not matched by rule when it should have been");
        assertEquals(result.get(), 100);
    }
    
    static void assertItemFail(ItemProperties item, RuleSet rule)
    {
        Optional<Integer> result = rule.determineStackSizeForItem(item);
        assertFalse(result.isPresent(), "Item matched by rule when it should not have been");
    }
    
    static void assertExceptionMessageStartsWith(String expected, Exception exception)
    {
        assertTrue(exception.getMessage().startsWith(expected),
                   "Expected <" + expected + "> at start, but <" + exception.getMessage() +
                           "> does not start with that."
        );
    }
    
    static RuleSet simpleRuleset(String condition)
    {
        return ConfigLib.parseRuleset(
                "<ruleset><rule stacksize=\"100\"><condition>" + condition + "</condition></rule></ruleset>");
    }
}
