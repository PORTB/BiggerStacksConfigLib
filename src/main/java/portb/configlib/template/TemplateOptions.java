/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.template;

public class TemplateOptions
{
    private final int normalStackLimit;
    private final int potionStackLimit;
    private final int enchBookLimit;
    
    public TemplateOptions(int normalStackLimit, int potionStackLimit, int enchBookLimit)
    {
        this.normalStackLimit = normalStackLimit;
        this.potionStackLimit = potionStackLimit;
        this.enchBookLimit = enchBookLimit;
    }
    
    public int getNormalStackLimit()
    {
        return normalStackLimit;
    }
    
    public int getPotionStackLimit()
    {
        return potionStackLimit;
    }
    
    public int getEnchBookLimit()
    {
        return enchBookLimit;
    }
    
    @Override
    public String toString()
    {
        return "TemplateOptions{" +
                       "normalStackLimit=" + normalStackLimit +
                       ", potionStackLimit=" + potionStackLimit +
                       ", enchBookLimit=" + enchBookLimit +
                       '}';
    }
}