/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.template;

import portb.configlib.xml.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfigTemplate extends TemplateOptions
{
    static final private String     TEMPLATE_COMMENT  = "\t⚠ This file was generated automatically ⚠\n" +
                                                                "\tYou should remove this comment if you want to modify this file or \"/biggerstacks quicksetup\" will overwrite it without warning\n";
    static final private Pattern    PARAMETER_PATTERN = Pattern.compile(
            TEMPLATE_COMMENT.replace("/", "\\/").replaceAll("\n$", "") + "\\s+\\[\\[(\\d+) (\\d+) (\\d+)]]");
    private final        List<Rule> rules;
    
    public ConfigTemplate(int normalStackLimit, int potionStackLimit, int enchBookLimit, List<Rule> rules)
    {
        super(normalStackLimit, potionStackLimit, enchBookLimit);
        this.rules = rules;
    }
    
    public static ConfigTemplate generateTemplate(TemplateOptions options)
    {
        ArrayList<Rule> rules = new ArrayList<>();
        
        rules.add(0,
                  new Rule(Collections.singletonList(new Condition(Property.STACK_SIZE, Operator.GREATER_THAN, 1)),
                           options.getNormalStackLimit()
                  )
        );
        
        if (options.getPotionStackLimit() != 1)
            rules.add(0,
                      new Rule(
                              Collections.singletonList(
                                      new OrBlock(Arrays.asList(
                                              new Condition(Property.ID, Operator.EQUALS, "minecraft:potion"),
                                              new Condition(Property.ID, Operator.EQUALS, "minecraft:splash_potion"),
                                              new Condition(Property.ID, Operator.EQUALS, "minecraft:lingering_potion")
                                      ))
                              ),
                              options.getPotionStackLimit()
                      )
            );
        
        if (options.getEnchBookLimit() != 1)
            rules.add(0,
                      new Rule(
                              Collections.singletonList(
                                      new Condition(Property.ID, Operator.EQUALS, "minecraft:enchanted_book")
                              ),
                              options.getEnchBookLimit()
                      )
            );
        
        return new ConfigTemplate(options.getNormalStackLimit(),
                                  options.getPotionStackLimit(),
                                  options.getEnchBookLimit(),
                                  rules
        );
    }
    
    public List<Rule> getRules()
    {
        return rules;
    }
    
    public String toXML()
    {
        return "<!--\n" +
                       TEMPLATE_COMMENT +
                       "\t[[" + getNormalStackLimit() + " " + getPotionStackLimit() + " " + getEnchBookLimit() +
                       "]]\n" +
                       "-->\n" +
                       new RuleSet(rules).toPrettyXML();
    }
    
    /**
     * Read the parameters from a template file that were used to create it
     *
     * @param templateFileText The contents of the template file
     * @return A TemplateOptions with normalStackLimit, potionStackLimit and enchBookLimit initialised.
     * @throws NumberFormatException    If the template has been tampered with or is otherwise invalid
     * @throws IllegalArgumentException If the template has been tampered with or is otherwise invalid
     */
    public static TemplateOptions readParametersFromTemplate(String templateFileText)
    {
        Matcher matcher = PARAMETER_PATTERN.matcher(templateFileText);
        
        if (!matcher.find())
            throw new IllegalArgumentException("Not a template file");
        
        return new TemplateOptions(Integer.parseInt(matcher.group(1)),
                                   Integer.parseInt(matcher.group(2)),
                                   Integer.parseInt(matcher.group(3))
        );
    }
}
