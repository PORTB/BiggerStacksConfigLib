/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.enums.EnumToStringConverter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public enum Property
{
    MOD_ID("mod", basicOperators()),
    ID("id",
       Arrays.asList(Operator.EQUALS, Operator.NOT_EQUALS, Operator.STRING_STARTS_WITH, Operator.STRING_ENDS_WITH)
    ),
    CATEGORY("category", basicOperators()),
    STACK_SIZE("stacksize",
               Arrays.asList(Operator.EQUALS,
                             Operator.NOT_EQUALS,
                             Operator.GREATER_THAN,
                             Operator.GREATER_THAN_EQUAL,
                             Operator.LESS_THAN,
                             Operator.LESS_THAN_EQUAL
               ),
               Property::parseStackSize
    ),
    EDIBLE("is_food", basicOperators(), Property::betterBooleanParser, Operator.EQUALS, true),
    PLACEABLE("is_block", basicOperators(), Property::betterBooleanParser, Operator.EQUALS, true),
    BUCKET("is_bucket", basicOperators(), Property::betterBooleanParser, Operator.EQUALS, true),
    TAGS("tag", Arrays.asList(Operator.EXCLUDES, Operator.INCLUDES), Operator.INCLUDES),
    CLASS("class",
          Arrays.asList(Operator.INSTANCE_OF, Operator.CLASS_NAME_EQUALS, Operator.CLASS_NAME_NOT_EQUALS),
          Operator.INSTANCE_OF
    ),
    DAMAGEABLE("is_tool", basicOperators(), Property::betterBooleanParser, Operator.EQUALS, true);
    //BLOCK_CLASS("block_class", CLASS.operatorContext, CLASS.defaultOperator);
    
    /**
     * Validates and parses a value from a string.
     * Used for the value/operand attribute on the condition block
     */
    private final Function<String, Object> validator;
    
    /**
     * What operators are allowed for this property
     */
    private final List<Operator> operatorContext;
    
    /**
     * The default operator if nothing is specified
     */
    private final Operator defaultOperator;
    
    /**
     * Used to convert string symbols to the respective operator object
     */
    private final Map<String, Operator> operatorSymbolMap;
    
    /**
     * The name of the property, used for parsing from xml
     */
    private final String name;
    
    /**
     * The default value to use if none was specified. Only used for booleans
     */
    private final Object defaultValue;
    
    Property(String name, List<Operator> operatorContext, Function<String, Object> validator)
    {
        this(name, operatorContext, validator, Operator.EQUALS, null);
    }
    
    Property(String name, List<Operator> operatorContext)
    {
        this(name, operatorContext, (it) -> it, Operator.EQUALS, null);
    }
    
    Property(String name, List<Operator> operatorContext, Operator defaultOperator)
    {
        this(name, operatorContext, (it) -> it, defaultOperator, null);
    }
    
    Property(String name, List<Operator> operatorContext, Function<String, Object> validator, Operator defaultOperator, Object defaultValue)
    {
        this.operatorContext = operatorContext;
        this.name = name;
        this.validator = validator;
        this.defaultOperator = defaultOperator;
        this.defaultValue = defaultValue;
        this.operatorSymbolMap = buildSymbolMap();
    }
    
    /**
     * Creates a symbol map to match string representation of operators
     */
    private Map<String, Operator> buildSymbolMap()
    {
        Map<String, Operator> map = new HashMap<>();
        
        for (Operator operator : operatorContext)
        {
            map.put(operator.symbol(), operator);
        }
        
        return map;
    }
    
    /**
     * Converts the value that will be compared against the property into the correct type for the operator
     */
    public Object convertValue(String value)
    {
        return validator.apply(value);
    }
    
    /**
     * Converts the operator used in a condition statement into its object form
     * e.g. "gt" becomes {@code Operator.GREATER_THAN}
     *
     * @param value The operator symbol
     * @return The operator object
     * @throws ConversionException if the operator could not be converted
     */
    public Operator convertOperator(String value)
    {
        value = value.toLowerCase();
        
        if (operatorSymbolMap.containsKey(value))
            return operatorSymbolMap.get(value);
        
        throw new ConversionException(
                "Operator \"" + value + "\" is invalid or can't be used with property \"" + name + "\"");
    }
    
    /**
     * Default operator when an explicit operator is omitted
     */
    public Operator getDefaultOperator()
    {
        return defaultOperator;
    }
    
    public String getName()
    {
        return name;
    }
    
    public Object getDefaultValue()
    {
        return defaultValue;
    }
    
    /**
     * Converts the string representation of the property into the property object
     */
    public final static EnumToStringConverter<Property> CONVERTER;
    
    static
    {
        final Map<String, Property> enumMap = new HashMap<>();
        
        for (Property value : values())
        {
            enumMap.put(value.name, value);
        }
        
        CONVERTER = new EnumToStringConverter<>(Property.class, enumMap);
    }
    
    private static Object parseStackSize(String val)
    {
        try
        {
            int intVal = Integer.parseInt(val);
            
            if (intVal < 0 || intVal > Integer.MAX_VALUE / 2)
                throw new ConversionException(Property.STACK_SIZE.name + " must be between 0 and " + Integer.MAX_VALUE);
            
            return intVal;
        }
        catch (NumberFormatException e)
        {
            throw new ConversionException("'" + val + "' is not a number");
        }
    }
    
    private static Object betterBooleanParser(String val)
    {
        val = val.toLowerCase();
        
        if (!val.equals("true") && !val.equals("false"))
            throw new IllegalArgumentException(val + " is not a valid boolean value");
        
        return Boolean.parseBoolean(val);
    }
    
    private static List<Operator> basicOperators()
    {
        return Arrays.asList(Operator.EQUALS, Operator.NOT_EQUALS);
    }
    
}
