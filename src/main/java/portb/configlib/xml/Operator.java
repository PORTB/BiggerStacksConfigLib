/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;

import com.thoughtworks.xstream.converters.enums.EnumToStringConverter;
import portb.configlib.TagAccessor;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;

public enum Operator
{
    EQUALS(new String[]{"=", "=="}, Objects::equals),
    NOT_EQUALS("!=", (a, b) -> !Objects.equals(a, b)),
    GREATER_THAN("gt", (a, b) -> ((Integer) a) > (Integer) b),
    GREATER_THAN_EQUAL("gte", (a, b) -> ((Integer) a) >= (Integer) b),
    LESS_THAN("lt", (a, b) -> ((Integer) a) < (Integer) b),
    LESS_THAN_EQUAL("lte", (a, b) -> ((Integer) a) <= (Integer) b),
    INCLUDES("includes", (tagAccessor, tag) -> ((TagAccessor) tagAccessor).doesItemHaveTag((String) tag)),
    EXCLUDES("excludes", (tagAccessor, tag) -> !INCLUDES.compare(tagAccessor, tag)),
    INSTANCE_OF("instanceof", Operator::instanceOfOperatorComparator),
    CLASS_NAME_EQUALS(new String[]{"=", "=="}, (a, b) -> {
        if (a == null)
            return false;
        
        return ((Class<?>) a).getName().equals(b);
    }),
    CLASS_NAME_NOT_EQUALS("!=", (a, b) -> !CLASS_NAME_EQUALS.compare(a, b)),
    STRING_STARTS_WITH("starts_with", (a, b) -> ((String) a).startsWith((String) b)),
    STRING_ENDS_WITH("ends_with", (a, b) -> ((String) a).endsWith((String) b));
    
    /**
     * The symbols that are used to identify the operator
     */
    private final String[]                            symbols;
    /**
     * The comparison function
     */
    private final BiFunction<Object, Object, Boolean> comparator;
    
    Operator(String symbol, BiFunction<Object, Object, Boolean> comparator)
    {
        this.symbols = new String[]{symbol};
        this.comparator = comparator;
    }
    
    Operator(String[] symbols, BiFunction<Object, Object, Boolean> comparator)
    {
        this.symbols = symbols;
        this.comparator = comparator;
    }
    
    /**
     * Does the comparison between 2 values
     */
    public boolean compare(Object lhs, Object rhs)
    {
        return comparator.apply(lhs, rhs);
    }
    
    /**
     * Gets the primary symbol used for the operator
     */
    public String symbol()
    {
        return symbols[0];
    }
    
    /**
     * Used for xstream
     */
    public final static EnumToStringConverter<Operator> CONVERTER;
    
    static
    {
        final Map<String, Operator> enumMap = new HashMap<>();
        
        for (Operator value : values())
        {
            for (String symbol : value.symbols)
                enumMap.put(symbol, value);
        }
        
        CONVERTER = new EnumToStringConverter<>(Operator.class, enumMap);
    }
    
    private static boolean instanceOfOperatorComparator(Object lhs, Object rhs)
    {
        //guard for possible null
        if (lhs == null)
            return false;
        
        return instanceOfOperatorComparator((Class<?>) lhs, (String) rhs);
    }
    
    private static boolean instanceOfOperatorComparator(Class<?> clazz, String className)
    {
        if (clazz == Object.class)
            return false;
        else if (clazz.getName().equals(className))
            return true;
        
        for (Class<?> interface_ : clazz.getInterfaces())
        {
            if (interface_.getName().equals(className))
                return true;
        }
        
        return instanceOfOperatorComparator(clazz.getSuperclass(), className);
    }
}
