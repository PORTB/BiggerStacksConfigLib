/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.SingleValueConverter;
import portb.configlib.ItemProperties;

import java.util.List;

@XStreamAlias("rule")
public class Rule //don't make this an EvaluableCondition or you will be able to nest rules in rules
{
    @XStreamImplicit
    private final List<EvaluableCondition> conditions;
    
    @XStreamAsAttribute
    @XStreamAlias("stacksize")
    @XStreamConverter(StackSizeConverter.class)
    private final int stackSize;
    
    public Rule(List<EvaluableCondition> conditions, int stackSize)
    {
        this.conditions = conditions;
        this.stackSize = stackSize;
    }
    
    public int getStackSize()
    {
        return stackSize;
    }
    
    public boolean matchesItem(ItemProperties properties)
    {
        return AndBlock.doesItemSatisfyAllProperties(properties, conditions);
    }
    
    @Override
    public String toString()
    {
        return "Rule{" +
                       "conditions=" + conditions +
                       ", stackSize=" + stackSize +
                       '}';
    }
    
    public static class StackSizeConverter implements SingleValueConverter
    {
        @Override
        public String toString(Object obj)
        {
            return obj.toString();
        }
        
        @Override
        public Object fromString(String str)
        {
            try
            {
                int value = Integer.parseInt(str);
                
                if(value <= 0)
                    throw new ConversionException("Stacksize must be greater than 0");
                else if(value > Integer.MAX_VALUE / 2)
                    throw new ConversionException("Stacksize must be less than " + (Integer.MAX_VALUE / 2));
                
                return value;
            }
            catch (NumberFormatException e)
            {
                throw new ConversionException(str + " is not a valid number", e);
            }
        }
        
        @Override
        public boolean canConvert(Class type)
        {
            return type.equals(Integer.class);
        }
    }
    
}
