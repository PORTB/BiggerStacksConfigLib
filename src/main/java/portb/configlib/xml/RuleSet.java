/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import portb.configlib.ConfigLib;
import portb.configlib.ItemProperties;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@XStreamAlias("ruleset")
public class RuleSet
{
    @XStreamImplicit
    private List<Rule> ruleList;
    
    public RuleSet(List<Rule> ruleList)
    {
        this.ruleList = ruleList;
    }
    
    /**
     * Determines the stack size for an item
     *
     * @param properties The item properties to match.
     * @return If there is a matching rule for the item, the stacksize. If there is no matching rules or if the item is a tool, empty.
     */
    public Optional<Integer> determineStackSizeForItem(ItemProperties properties)
    {
        if (ruleList == null)
            return Optional.empty();
        
        //hardcoded rule for not stacking tools and buckets has been removed for now
        /*if (properties.isDamageable
                    || (properties.isBucket && !properties.itemId.equals("minecraft:bucket"))
        )
            return Optional.empty();
        else*/
        if (properties.itemId.equals("minecraft:air"))
            return Optional.empty();
        
        for (Rule rule : ruleList)
        {
            if (rule.matchesItem(properties))
                return Optional.of(rule.getStackSize());
        }
        
        return Optional.empty();
    }
    
    /**
     * Adds rules to the ruleset.
     * Used to add rules created by other mods to the ruleset.
     *
     * @param extraRules The rules to add to the ruleset
     */
    public void addRules(List<Rule> extraRules)
    {
        try
        {
            if (ruleList == null)
                ruleList = extraRules;
            else
                ruleList.addAll(extraRules);
        }
        catch (UnsupportedOperationException e)
        {
            //xstream uses some type of list that doesn't support adding, so convert to arraylist
            ruleList = new ArrayList<>(ruleList);
            
            ruleList.addAll(extraRules);
        }
    }
    
    public int getMaxStacksize()
    {
        //start at 64 to prevent lowering the global stack size and messing up items that would normally stack to 64
        int max = 64;
        
        if (ruleList != null)
        {
            for (Rule rule : ruleList)
            {
                max = Math.max(rule.getStackSize(), max);
            }
        }
        
        return max;
    }
    
    public List<Rule> getRuleList()
    {
        return ruleList;
    }
    
    @Override
    public String toString()
    {
        return "RuleSet{" +
                       "ruleList=" + ruleList +
                       '}';
    }
    
    public String toPrettyXML()
    {
        return ConfigLib.prettyXStream().toXML(this);
    }
    
    public byte[] toBytes()
    {
        //converting the object back to xml is more efficient than using java serializable
        return ConfigLib.xStream().toXML(this).getBytes(StandardCharsets.UTF_8);
    }
    
    public static RuleSet fromBytes(byte[] bytes)
    {
        //converting the xml to object is more efficient than using java serializable
        String str = new String(bytes, StandardCharsets.UTF_8);
        return (RuleSet) ConfigLib.xStream().fromXML(str);
    }
}
