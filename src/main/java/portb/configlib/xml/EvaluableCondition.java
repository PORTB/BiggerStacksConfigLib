/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;


import portb.configlib.ItemProperties;

public interface EvaluableCondition
{
    boolean isSatisfiedByItem(ItemProperties itemProperties);
}
