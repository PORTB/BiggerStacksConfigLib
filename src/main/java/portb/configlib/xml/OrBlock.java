/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import portb.configlib.ItemProperties;

import java.util.List;

@XStreamAlias("or")
public class OrBlock implements EvaluableCondition
{
    @SuppressWarnings({"unused", "MismatchedQueryAndUpdateOfCollection"})
    @XStreamImplicit
    private List<EvaluableCondition> conditions;
    
    public OrBlock() {}
    
    public OrBlock(List<EvaluableCondition> conditions)
    {
        this.conditions = conditions;
    }
    
    @Override
    public boolean isSatisfiedByItem(ItemProperties itemProperties)
    {
        for (EvaluableCondition condition : conditions)
        {
            if (condition.isSatisfiedByItem(itemProperties))
                return true;
        }
        
        return false;
    }
    
    @Override
    public String toString()
    {
        return "OrBlock{" +
                       "conditions=" + conditions +
                       '}';
    }
}
