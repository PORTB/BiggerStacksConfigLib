/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import portb.configlib.ItemProperties;

/**
 * The condition block
 */
@XStreamAlias("condition")
public class Condition implements EvaluableCondition
{
    /**
     * The property of the item that we are interested in comparing to the operand
     * The left hand side value that we are applying the operator to
     */
    private Property property;
    /**
     * The operator we should apply
     */
    private Operator operator;
    /**
     * The right hand side value that we are applying the operator to
     */
    @XStreamAsAttribute
    private Object   operand;
    
    public Condition() {}
    
    public Condition(Property property, Operator operator, Object operand)
    {
        this.property = property;
        this.operator = operator;
        this.operand = operand;
    }
    
    @Override
    public boolean isSatisfiedByItem(ItemProperties item)
    {
        return operator.compare(item.getProperty(property), operand);
    }
    
    public static class ConditionConverter implements Converter
    {
        @Override
        public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context)
        {
            Condition condition = (Condition) source;
            
            writer.setValue(condition.property.getName() + " " + condition.operator.symbol() + " " +
                                    condition.operand.toString());
        }
        
        @Override
        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context)
        {
            final String[] entities = extractEntitiesFromCondition(reader.getValue());
            
            Condition condition = new Condition();
            
            try
            {
                condition.property = (Property) Property.CONVERTER.fromString(entities[0]);
            }
            catch (ConversionException e)
            {
                throw new ConversionException("Property \"" + entities[0] + "\" is invalid", e);
            }
            
            //shortcut for boolean properties
            //for example, allows <condition>is_food</condition>
            if (entities.length == 1)
            {
                if (condition.property.getDefaultValue() == null)
                    throw new ConversionException("Property \"" + entities[0] + "\" requires a value to compare to");
                
                condition.operator = condition.property.getDefaultOperator();
                condition.operand = condition.property.getDefaultValue();
            }
            //shortcut for using the default operator
            //for example, allows <condition>stacksize 16</condition>
            else if (entities.length == 2)
            {
                condition.operator = condition.property.getDefaultOperator();
                condition.operand = condition.property.convertValue(entities[1]);
            }
            //full syntax, no shortcuts
            else if (entities.length == 3)
            {
                condition.operator = condition.property.convertOperator(entities[1]);
                condition.operand = condition.property.convertValue(entities[2]);
            }
            
            return condition;
        }
        
        private String[] extractEntitiesFromCondition(String conditionContent)
        {
            if (conditionContent == null || conditionContent.isEmpty())
                throw new ConversionException("Condition is empty");
            
            String[] entities = conditionContent.trim().split("\\s");
            
            if (entities.length > 3)
                throw new ConversionException("Too many entities in \"" + conditionContent + "\"");
            
            return entities;
        }
        
        @Override
        public boolean canConvert(Class type)
        {
            return type.equals(Condition.class);
        }
    }
    
    @Override
    public String toString()
    {
        return "Condition{" +
                       "property=" + property +
                       ", operator=" + operator +
                       ", operand=" + operand +
                       '}';
    }
}
