/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import portb.configlib.ItemProperties;

import java.util.List;

@XStreamAlias("and")
public class AndBlock implements EvaluableCondition
{
    @SuppressWarnings("unused")
    @XStreamImplicit
    private List<EvaluableCondition> conditions;
    
    @Override
    public boolean isSatisfiedByItem(ItemProperties itemProperties)
    {
        return doesItemSatisfyAllProperties(itemProperties, conditions);
    }
    
    public static boolean doesItemSatisfyAllProperties(ItemProperties itemProperties, List<EvaluableCondition> conditions)
    {
        for (EvaluableCondition condition: conditions)
        {
            if (!condition.isSatisfiedByItem(itemProperties))
                return false;
        }
        
        return true;
    }
    
    @Override
    public String toString()
    {
        return "AndBlock{" +
                       "conditions=" + conditions +
                       '}';
    }
}
