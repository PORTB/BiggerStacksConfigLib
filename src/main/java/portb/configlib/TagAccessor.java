/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib;

public interface TagAccessor
{
    boolean doesItemHaveTag(String tag);
}
