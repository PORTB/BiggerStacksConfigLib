/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import portb.configlib.xml.*;
import portb.slw.MyLogger;
import portb.slw.MyLoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

public class ConfigLib
{
    /**
     * xstream instance
     */
    private static final XStream xStream;
    //creates readable xml for templates
    //use separate xstream for pretty printing so that the tests don't break
    private static final XStream xStreamPretty;
    
    public static MyLogger LOGGER = MyLoggerFactory.emptyLogger();
    
    //Configure xstream
    static
    {
        xStream = new XStream(new StaxDriver());
        xStreamPretty = new XStream(new StaxDriver()
        {
            //use pretty printer so that template files are actually readable
            @Override
            public HierarchicalStreamWriter createWriter(Writer out)
            {
                return new PrettyPrintWriter(out, "\t");
            }
        });
        
        initXStream(xStream);
        initXStream(xStreamPretty);
    }
    
    private static void initXStream(XStream xStream)
    {
        //only allow our xml types
        xStream.allowTypesByWildcard(new String[]{"portb.configlib.xml.*"});
        xStream.processAnnotations(new Class[]{Condition.class,
                                               Property.class,
                                               AndBlock.class,
                                               OrBlock.class,
                                               Rule.class,
                                               RuleSet.class});
        
        //register converters
        xStream.registerConverter(Property.CONVERTER);
        xStream.registerConverter(Operator.CONVERTER);
        xStream.registerConverter(new Condition.ConditionConverter());
        xStream.registerConverter(new Rule.StackSizeConverter());
    }
    
    public static RuleSet parseRuleset(File file)
    {
        return (RuleSet) xStream.fromXML(file);
    }
    
    public static RuleSet parseRuleset(String xml)
    {
        return (RuleSet) xStream.fromXML(xml);
    }
    
    public static XStream xStream()
    {
        return xStream;
    }
    
    public static XStream prettyXStream() {return xStreamPretty;}
    
    public static RuleSet getFallbackRuleset()
    {
        return new RuleSet(Collections.emptyList());
    }
    
    public static RuleSet readRuleset(Path xmlFile)
    {
        RuleSet result;
        
        if (Files.exists(xmlFile))
        {
            LOGGER.info("Reading stacksize ruleset");
            
            try
            {
                result = (RuleSet) xStream.fromXML(new String(Files.readAllBytes(xmlFile), StandardCharsets.UTF_8));
            }
            catch (IOException e)
            {
                throw new RuntimeException("Could not read stacksize ruleset", e);
            }
        }
        else
        {
            LOGGER.info("Could not find config, falling back to default legacy ruleset");
            result = ConfigLib.getFallbackRuleset();
        }
        
        result.addRules(IMCAPI.collectIMCRules());
        
        return result;
    }
}
