/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib;

import com.thoughtworks.xstream.XStreamException;
import portb.configlib.xml.Rule;
import portb.configlib.xml.RuleSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static portb.configlib.ConfigLib.LOGGER;

public class IMCAPI
{
    private static final ArrayList<IMCRuleSupplier> IMC_RULE_SUPPLIERS = new ArrayList<>();
    
    /**
     * Add a rule supplier sent to the mod via IMC to be used as rule supplier
     *
     * @param source   The ID of the mod that sent the IMC message
     * @param supplier The supplier which produces the rules
     */
    public static void addIMCRuleSupplier(String source, Supplier<?> supplier)
    {
        IMC_RULE_SUPPLIERS.add(new IMCRuleSupplier(source, supplier));
    }
    
    static List<Rule> collectIMCRules()
    {
        return IMC_RULE_SUPPLIERS.stream()
                                 .flatMap(IMCAPI::convertIMCMessageToRule)
                                 .collect(Collectors.toList());
    }
    
    /**
     * Converts an IMC message of a single Rule, a Collection<Rule>, a RuleSet (from xml) or a single Rule (from xml)
     *
     * @param imcRuleSupplier Supplier sent to this mod via IMC which produces xml string, rule collection or rule
     * @return The rule that the message contained, or Stream.empty() if it was invalid
     */
    private static Stream<Rule> convertIMCMessageToRule(IMCRuleSupplier imcRuleSupplier)
    {
        Object message = imcRuleSupplier.supplier.get();
        
        //single rule
        if (message instanceof Rule)
        {
            return Stream.of((Rule) message);
        }
        //list of rules
        else if (message instanceof Collection<?>)
        {
            List<Rule> ruleList = new ArrayList<>();
            
            for (Object item : ((Collection<?>) message))
            {
                if (item instanceof Rule)
                    ruleList.add((Rule) item);
                else
                    LOGGER.warn("Expected IMC supplier to produce List<Rule>, but list contained item of type " +
                                        item.getClass().getName() + ". Source: " + imcRuleSupplier.source);
            }
            
            return ruleList.stream();
        }
        //message containing XML
        else if (message instanceof String)
        {
            try
            {
                //decode the XML
                Object xmlObject = ConfigLib.xStream().fromXML((String) message);
                
                //return the decoded object as a stream
                if (xmlObject instanceof Rule)
                    return Stream.of((Rule) xmlObject);
                else if (xmlObject instanceof RuleSet)
                    return ((RuleSet) xmlObject).getRuleList().stream();
                else
                    LOGGER.warn("Expected IMC supplier to produce ruleset or rule in XML, but got " +
                                        xmlObject.getClass().getName() + ". Source: " + imcRuleSupplier.source);
            }
            catch (XStreamException e)
            {
                LOGGER.error("Error when trying to parse IMC message. Source: " + imcRuleSupplier.source, e);
            }
        }
        else
        {
            LOGGER.warn("Expected IMC supplier to produce List<Rule>, Rule or a string containing XML, but got " +
                                message.getClass().getName() +
                                ". Source: " + imcRuleSupplier.source);
        }
        
        //fail without crashing the whole game
        return Stream.empty();
    }
    
    private static class IMCRuleSupplier
    {
        final String      source;
        final Supplier<?> supplier;
        
        public IMCRuleSupplier(String source, Supplier<?> supplier)
        {
            this.source = source;
            this.supplier = supplier;
        }
    }
}
