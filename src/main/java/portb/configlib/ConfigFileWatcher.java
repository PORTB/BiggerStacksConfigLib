/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib;

import com.thoughtworks.xstream.XStreamException;
import portb.configlib.xml.RuleSet;

import java.io.IOException;
import java.nio.file.*;
import java.util.function.Consumer;

import static portb.configlib.ConfigLib.LOGGER;
import static portb.configlib.ConfigLib.xStream;

/**
 * Watches the xml config file for updates and invokes a callback when it changes
 */
@SuppressWarnings("unused")
public class ConfigFileWatcher
{
    private final Thread            watcherThread;
    private final Path              configFile;
    private       Consumer<RuleSet> onUpdateAction;
    private       boolean           running = true;
    private       WatchService      watcher;
    
    /**
     * Creates a config file watcher
     *
     * @param configFile The file to watch
     */
    public ConfigFileWatcher(Path configFile)
    {
        this.configFile = configFile;
        this.watcherThread = new Thread(this::watcherLoop, "Config file watcher thread");
        
        try
        {
            watcher = FileSystems.getDefault().newWatchService();
            
            configFile.getParent().register(watcher,
                                            StandardWatchEventKinds.ENTRY_MODIFY,
                                            //StandardWatchEventKinds.ENTRY_CREATE,
                                            StandardWatchEventKinds.ENTRY_DELETE
            );
        }
        catch (IOException e)
        {
            running = false;
            
            LOGGER.error("Could not create watcher", e);
        }
    }
    
    /**
     * Starts the watcher
     */
    public void start()
    {
        watcherThread.start();
    }
    
    //Executed on the watcher thread
    private void watcherLoop()
    {
        LOGGER.info("Starting config file watcher");
        
        while (running)
        {
            try
            {
                WatchKey key = watcher.take();
                
                for (WatchEvent<?> _event : key.pollEvents())
                {
                    
                    @SuppressWarnings("unchecked")
                    WatchEvent<Path> event = (WatchEvent<Path>) _event;
                    WatchEvent.Kind<Path> kind     = event.kind();
                    String                fileName = event.context().getFileName().toString();
                    
                    
                    if (configFile.getFileName().toString().equals(fileName))
                    {
                        if (kind == StandardWatchEventKinds.ENTRY_MODIFY ||
                                    kind == StandardWatchEventKinds.ENTRY_CREATE)
                        {
                            if (onUpdateAction != null)
                                try
                                {
                                    //the event always seems to give the incorrect location of the file, it's missing the last directory
                                    //e.g. C:/somefolder/anotherfolder/thing.xml
                                    //becomes C:/somefolder/thing.xml.
                                    //i'm not sure why. just store the actual file path we want to watch and use it to read the file
                                    onUpdateAction.accept((RuleSet) xStream().fromXML(configFile.toFile()));
                                }
                                catch (XStreamException e)
                                {
                                    LOGGER.error("Config file was unable to be parsed", e);
                                }
                            
                            LOGGER.info("Config file updated");
                        }
                        else if (kind == StandardWatchEventKinds.ENTRY_DELETE)
                        {
                            LOGGER.info("Config file deleted, now using fallback config");
                            if (onUpdateAction != null)
                                onUpdateAction.accept(ConfigLib.getFallbackRuleset());
                        }
                    }
                    else
                    {
                        LOGGER.debug(
                                "Got " + event.kind().name() + " for " + fileName + ", ignoring");
                    }
                }
                
                if (!key.reset())
                {
                    LOGGER.warn("WatchKey could not be reset");
                    return;
                }
            }
            catch (InterruptedException e)
            {
                LOGGER.error("Config file watcher was interrupted", e);
                return;
            }
            catch (ClosedWatchServiceException e)
            {
                LOGGER.info("Watcher has shut down");
                return;
            }
        }
        
        LOGGER.warn("Watcher has shut down, potentially due to an error");
    }
    
    public void setOnUpdateAction(Consumer<RuleSet> onUpdateAction)
    {
        this.onUpdateAction = onUpdateAction;
    }
    
    public void stop()
    {
        try
        {
            running = false;
            watcher.close();
        }
        catch (IOException e)
        {
            LOGGER.error("Could not stop watcher", e);
        }
    }
}

