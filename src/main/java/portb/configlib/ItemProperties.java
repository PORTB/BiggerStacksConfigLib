/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.configlib;

import portb.configlib.xml.Property;

public class ItemProperties
{
    public final  String      modId;
    public final  String      itemId;
    public final  String      itemCategory;
    public final  Integer     maxStackSize;
    public final  Boolean     isEdible;
    public final  Boolean     isPlaceable;
    public final  Boolean     isDamageable;
    public final  Boolean     isBucket;
    //public final Collection<String> tags;
    public final  Class<?>    clazz;
    private final TagAccessor tagAccessor;
    
    //hopefully tags will be able to do what this was going to do
    //public final Class<?> blockClass;
    
    
    public ItemProperties(String modId, String itemId, String itemCategory, Integer maxStackSize, Boolean isEdible, Boolean isPlaceable, Boolean isDamageable, Boolean isBucket, TagAccessor tagAccessor, Class<?> clazz)
    {
        this.modId = modId;
        this.itemId = itemId;
        this.itemCategory = itemCategory;
        this.maxStackSize = maxStackSize;
        this.isEdible = isEdible;
        this.isPlaceable = isPlaceable;
        this.isDamageable = isDamageable;
        this.isBucket = isBucket;
        this.tagAccessor = tagAccessor;
        this.clazz = clazz;
    }
    
    public Object getProperty(final Property property)
    {
        switch (property)
        {
            case MOD_ID:
                return modId;
            case ID:
                return itemId;
            case CATEGORY:
                return itemCategory;
            case STACK_SIZE:
                return maxStackSize;
            case EDIBLE:
                return isEdible;
            case PLACEABLE:
                return isPlaceable;
            case BUCKET:
                return isBucket;
            case TAGS:
                return tagAccessor;
            case CLASS:
                return clazz;
            case DAMAGEABLE:
                return isDamageable;
            default:
                throw new IllegalStateException("A switch case is missing");
        }
    }

//    public Collection<String> getTags()
//    {
//        if(tags == null)
//            tags = lazyTags.get();
//
//        return tags;
//
//    }
}
